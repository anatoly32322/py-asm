# py-asm

Лабораторная работа 3 по АК

______

## Выполнил
Бритков Анатолий Анатольевич

## Вариант
asm | acc | neum | hw | instr | struct | trap | port | prob5

- ЯП. Синтаксис: **asm**
  - Синтаксис ассемблера
  - Необходима поддержка label-ов
- Архитектура: **acc**
  - Cистема команд должна быть выстроена вокруг аккумулятора.
- Организация памяти: **neum**
  - Архитектура Фон-Неймана
- Control Unit: **hw** 
  - hardwided
  - Реализуется как часть модели
- Точность модели: **instr**
  - Процессор необходимо моделировать с точностью до каждой инструкции (наблюдается состояние после каждой инструкции).
- Представление машинного кода: **struct**
  - В виде высокоуровневой структуры данных
  - Считается, что одна инструкция укладывается в одно машинное слово, за исключением CISC архитектур
  - **ПРИМЕЧАНИЕ**. Транслятор переводит исходный код в класс `Program`, который затем сериализуется в бинарный вид. Компьютер использует десериализованный класс `Program`.
- Ввод-вывод: **trap**
  - Ввод-вывод осуществляется токенами через систему прерываний.
- Ввод-вывод ISA: **port**
  - memory-mapped
- Алгоритм: **prob5**
  - Входные данные должны подаваться через ввод
  - Результат должен быть подан на вывод
  - Формат ввода/вывода данных - на ваше усмотрение

## Организация памяти


## Язык программирования
### Структура программы
```
; Секция данных
section .data
    <var>: (STR) | (INT)
; Секция кода
section .text
    [label: ] instr [op1, [op2, [...]]]
```
### Поддержка комментариев
Чтобы вставить комментарий, просто напишите `;` и всё, что после неё - буде закомментировано
### Инструкции
Поддерживаются основные инструкции языка Assembler:
- XOR
- ADD
- SUB
- INC
- MOD
- MUL
- CMP
- JE
- JG
- JMP
- JNE
- MOV
- HLT
## Операнды
### Переменные
Переменные объявляются в секции данных и могу использоваться в дальнейшей программе.
Чтобы использовать переменную как операнд в инструкции, нужно перед название переменной написать '#'
```
...
    INC #X
...
```
### Регистры
```
...
    INC %rsx
...
```
### Константы
```
...
    ADD %rsx, 1
...
```
## Транслятор
Программа транслируется в три объектных файла program.o, program.mem, program.lab.
### program.o
Объектный файл хранит байт-код всех инструкций, которые будут выполнены
### program.mem
Объектный файл хранит байт-код памяти
### program.lab
Объектный файл хранит байт-код всех label'ов
## Транслирование
### Пример трансляции программы prob5
```
[Instruction(cmd='XOR',
             operands=[Register(name='rax'), Register(name='rax')],
             addr=0),
 Instruction(cmd='MOV',
             operands=[Label(label='MAX_DIVIDER', indirect_ptr=None),
                       Label(label='STDIN', indirect_ptr=None)],
             addr=1),
 Instruction(cmd='MOV',
             operands=[Register(name='rax'),
                       Label(label='STEP', indirect_ptr=None)],
             addr=2),
 Instruction(cmd='INC', operands=[Register(name='rax')], addr=3),
 Instruction(cmd='CMP',
             operands=[Register(name='rax'),
                       Label(label='MAX_DIVIDER', indirect_ptr=None)],
             addr=4),
 Instruction(cmd='JG',
             operands=[Label(label='find_number', indirect_ptr=None)],
             addr=5),
 Instruction(cmd='MOV', operands=[Register(name='rdx'), 2], addr=6),
 Instruction(cmd='MOD',
             operands=[Register(name='rbx'),
                       Register(name='rax'),
                       Register(name='rdx')],
             addr=7),
 Instruction(cmd='JE',
             operands=[Label(label='find_prime', indirect_ptr=None)],
             addr=8),
 Instruction(cmd='INC', operands=[Register(name='rdx')], addr=9),
 Instruction(cmd='CMP',
             operands=[Register(name='rax'), Register(name='rdx')],
             addr=10),
 Instruction(cmd='JE',
             operands=[Label(label='mul_step', indirect_ptr=None)],
             addr=11),
 Instruction(cmd='JMP',
             operands=[Label(label='check_mod', indirect_ptr=None)],
             addr=12),
 Instruction(cmd='MUL',
             operands=[Label(label='STEP', indirect_ptr=None),
                       Register(name='rax')],
             addr=13),
 Instruction(cmd='JMP',
             operands=[Label(label='find_prime', indirect_ptr=None)],
             addr=14),
 Instruction(cmd='XOR',
             operands=[Register(name='rax'), Register(name='rax')],
             addr=15),
 Instruction(cmd='ADD',
             operands=[Register(name='rax'),
                       Label(label='STEP', indirect_ptr=None)],
             addr=16),
 Instruction(cmd='XOR',
             operands=[Register(name='rdx'), Register(name='rdx')],
             addr=17),
 Instruction(cmd='INC', operands=[Register(name='rdx')], addr=18),
 Instruction(cmd='MOD',
             operands=[Register(name='rbx'),
                       Register(name='rax'),
                       Register(name='rdx')],
             addr=19),
 Instruction(cmd='JNE',
             operands=[Label(label='next_number', indirect_ptr=None)],
             addr=20),
 Instruction(cmd='CMP',
             operands=[Register(name='rdx'),
                       Label(label='MAX_DIVIDER', indirect_ptr=None)],
             addr=21),
 Instruction(cmd='JE',
             operands=[Label(label='exit', indirect_ptr=None)],
             addr=22),
 Instruction(cmd='JMP',
             operands=[Label(label='next_divider', indirect_ptr=None)],
             addr=23),
 Instruction(cmd='MOV',
             operands=[Label(label='STDOUT', indirect_ptr=None),
                       Register(name='rax')],
             addr=24),
 Instruction(cmd='HLT', operands=[None], addr=25)]
```
### Пример трансляции label'ов для prob5
```
{'MAX_DIVIDER': 0,
 'STEP': 1,
 'check_mod': 7,
 'exit': 24,
 'find_number': 15,
 'find_prime': 3,
 'mul_step': 13,
 'next_divider': 18,
 'next_number': 16}
```
## Логирование
Логирование выполненено с использованием модуля logging
### Пример логов
```
2023-01-19 15:02:02,929 MainProcess 50672 [INFO    ] cli: Translate program into object file
2023-01-19 15:02:02,929 MainProcess 50672 [INFO    ] cli: Execute program from object file
2023-01-19 15:02:02,929 MainProcess 50672 [DEBUG   ] machine: Opcode.MOV with [Register(name='rsx'), Label(label='STDIN')]
2023-01-19 15:02:02,930 MainProcess 50672 [INFO    ] machine: Get char from input buffer: 102
2023-01-19 15:02:02,930 MainProcess 50672 [DEBUG   ] machine: Opcode.CMP with [Register(name='rsx'), Label(label='NULL_TERM')]
2023-01-19 15:02:02,930 MainProcess 50672 [DEBUG   ] machine: Opcode.JE with [Label(label='exit')]
2023-01-19 15:02:02,930 MainProcess 50672 [DEBUG   ] machine: Opcode.MOV with [Label(label='STDOUT'), Register(name='rsx')]
2023-01-19 15:02:02,930 MainProcess 50672 [DEBUG   ] machine: Opcode.JMP with [Label(label='read_char')]
2023-01-19 15:02:02,930 MainProcess 50672 [DEBUG   ] machine: Opcode.MOV with [Register(name='rsx'), Label(label='STDIN')]
2023-01-19 15:02:02,930 MainProcess 50672 [INFO    ] machine: Get char from input buffer: 111
2023-01-19 15:02:02,930 MainProcess 50672 [DEBUG   ] machine: Opcode.CMP with [Register(name='rsx'), Label(label='NULL_TERM')]
2023-01-19 15:02:02,930 MainProcess 50672 [DEBUG   ] machine: Opcode.JE with [Label(label='exit')]
2023-01-19 15:02:02,930 MainProcess 50672 [DEBUG   ] machine: Opcode.MOV with [Label(label='STDOUT'), Register(name='rsx')]
2023-01-19 15:02:02,930 MainProcess 50672 [DEBUG   ] machine: Opcode.JMP with [Label(label='read_char')]
2023-01-19 15:02:02,930 MainProcess 50672 [DEBUG   ] machine: Opcode.MOV with [Register(name='rsx'), Label(label='STDIN')]
2023-01-19 15:02:02,930 MainProcess 50672 [INFO    ] machine: Get char from input buffer: 111
2023-01-19 15:02:02,930 MainProcess 50672 [DEBUG   ] machine: Opcode.CMP with [Register(name='rsx'), Label(label='NULL_TERM')]
2023-01-19 15:02:02,930 MainProcess 50672 [DEBUG   ] machine: Opcode.JE with [Label(label='exit')]
2023-01-19 15:02:02,930 MainProcess 50672 [DEBUG   ] machine: Opcode.MOV with [Label(label='STDOUT'), Register(name='rsx')]
2023-01-19 15:02:02,930 MainProcess 50672 [DEBUG   ] machine: Opcode.JMP with [Label(label='read_char')]
2023-01-19 15:02:02,930 MainProcess 50672 [DEBUG   ] machine: Opcode.MOV with [Register(name='rsx'), Label(label='STDIN')]
2023-01-19 15:02:02,930 MainProcess 50672 [INFO    ] machine: Reach end of input buffer
2023-01-19 15:02:02,930 MainProcess 50672 [DEBUG   ] machine: Opcode.CMP with [Register(name='rsx'), Label(label='NULL_TERM')]
2023-01-19 15:02:02,930 MainProcess 50672 [DEBUG   ] machine: Opcode.JE with [Label(label='exit')]
2023-01-19 15:02:02,930 MainProcess 50672 [DEBUG   ] machine: Opcode.HLT with [None]
2023-01-19 15:02:02,930 MainProcess 50672 [INFO    ] machine: tick: 28 ; instr: 19
```
## Апробация
### Реализации алгоритмов
Были реализованы три алгоритма:

[hello](examples/hello.asm)

[cat](examples/cat.asm)

[prob5](examples/prob5.asm)

### Unit-test
Были реализованы несколько юнит-тестов на проверку работоспособности транслятора и машины

### Integration-test
Проверяют работоспособность реализованных алгоритмов

### CI
Локально CI можно запустить с помощью [ci.sh](ci.sh)
```
#!/bin/bash

echo '=====PYLINT====='
pylint ./cli.py ./model.py ./translator.py ./util.py ./machine/
echo '======MYPY======'
mypy .

echo '==[TESTING]=='
echo '====UNIT-TESTS===='
python -m unittest test/unit/test_translator.py
python -m unittest test/unit/test_machine.py
python -m unittest test/integration/test_prob5.py
python -m unittest test/integration/test_cat.py
python -m unittest test/integration/test_hello.py
```

В GitLab CI запускаются с использованием [.gitlab-ci.yml](.gitlab-ci.yml)
```
image:
  name: python-tools

before_script:
  - pip install click
  - apt install mypy
  - apt install pylint

stages:
  - Analysis
  - Testing

pylint:
  stage: Analysis
  script:
    - pylint ./cli.py ./model.py ./translator.py ./util.py ./machine/

mypy:
  stage: Analysis
  script:
    - mypy .

test:
  stage: Testing
  script:
    - python3 -m unittest test/unit/test_translator.py
    - python3 -m unittest test/unit/test_machine.py
    - python3 -m unittest test/integration/test_prob5.py
    - python3 -m unittest test/integration/test_cat.py
    - python3 -m unittest test/integration/test_hello.py
```

```
=====PYLINT=====
************* Module cli

------------------------------------------------------------------
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)

======MYPY======
Success: no issues found in 19 source files
==[TESTING]==
====UNIT-TESTS====
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
...
----------------------------------------------------------------------
Ran 3 tests in 0.000s

OK
...
----------------------------------------------------------------------
Ran 3 tests in 0.236s

OK
.
----------------------------------------------------------------------
Ran 1 test in 0.064s

OK
.
----------------------------------------------------------------------
Ran 1 test in 0.066s

OK

```

## Результат работы программы
| ФИО         | алгоритм | инстр | такт |
|-------------|----------|-------|------|
| Бритков А.А | cat      | 19    | 28   |
|             | hello    | 34    | 56   |
|             | prob5    | 1744  | 3892 |
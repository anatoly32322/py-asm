"""
Utils
"""
import os
import pickle
import re

from model import Labels, Memory, Program

RE_STR = r'^(\'.*\')|(\".*\")$'


def is_string(string: str) -> bool:
    """
    Check if string is symbols surrounded by quotes
    """
    return bool(
        re.fullmatch(
            RE_STR,
            string
        )
    )



def parse_str_to_int(char: str) -> int:
    """
    Parse char to int
    """
    char = char.strip()
    if char.isdigit():
        return int(char)
    return ord(char)


def write_memory(memory: Memory, filepath: str) -> None:
    """
    Write memory into object file
    :param memory:
    :param filepath:
    :return:
    """
    asm_file, _ = filepath.split('.')
    with open(f'{asm_file}.mem', 'wb+') as object_file:
        pickle.dump(memory, object_file, protocol=pickle.HIGHEST_PROTOCOL)


def write_labels(labels: Labels, filepath: str) -> None:
    """
    Write labels into object-file
    :param labels:
    :param filepath:
    :return:
    """
    asm_file, _ = filepath.split('.')
    with open(f'{asm_file}.lab', 'wb+') as object_file:
        pickle.dump(labels, object_file, protocol=pickle.HIGHEST_PROTOCOL)


def write_program(program: Program, filepath: str) -> None:
    """
    Write program into object file
    :param program:
    :param filepath:
    :return:
    """
    asm_file, _ = filepath.split('.')
    with open(f'{asm_file}.o', 'wb+') as object_file:
        pickle.dump(program, object_file, protocol=pickle.HIGHEST_PROTOCOL)


def read_memory(filepath: str) -> Memory:
    """
    Read memory from object file
    :param filepath:
    :return:
    """
    asm_file, _ = filepath.split('.')
    with open(f'{asm_file}.mem', 'rb') as object_file:
        return pickle.load(object_file)


def read_labels(filepath: str) -> Labels:
    """
    Read labels from object file
    :param filepath:
    :return:
    """
    asm_file, _ = filepath.split('.')
    with open(f'{asm_file}.lab', 'rb') as object_file:
        return pickle.load(object_file)


def read_program(filepath: str) -> Program:
    """
    Read program from object file
    :param filepath:
    :return:
    """
    asm_file, _ = filepath.split('.')
    with open(f'{asm_file}.o', 'rb') as object_file:
        return pickle.load(object_file)


def delete_object_files(filepath: str) -> None:
    """
    Delete object files
    :param filepath:
    :return:
    """
    asm_file, _ = filepath.split('.')
    os.remove(f'{asm_file}.o')
    os.remove(f'{asm_file}.lab')
    os.remove(f'{asm_file}.mem')

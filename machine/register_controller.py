"""
Register controller
"""
from typing import Dict, Iterator

from machine.config import __available_registers__
from model import Register


class RegisterController:
    """
    Register Controller class
    """

    def __init__(self) -> None:
        """
        Initialize states for registers
        """
        self.__states__: Dict[str, int] = {
            register: 0 for register in self.keys()
        }

    def get(self, register: Register) -> int:
        """
        Get readable register value
        """
        return self.__states__[register.name]

    def set(self, register: Register, value: int) -> None:
        """
        Set writable register value
        """
        self.__states__[register.name] = value

    @staticmethod
    def keys() -> Iterator[str]:
        """
        Generate names of available registers
        """
        yield from __available_registers__

    def __repr__(self) -> str:
        return str(self.__states__)

    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super(RegisterController, cls).__new__(cls)
        return cls.instance

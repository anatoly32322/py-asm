"""
Machine
"""
# pylint: disable=too-many-branches
# pylint: disable=too-many-statements
# pylint: disable=too-many-instance-attributes
# pylint: disable=invalid-name
# pylint: disable=unused-variable
from copy import deepcopy
from typing import Any, Dict, List, Tuple, Union
from exceptions.exeptions import ProgramExit, IncorrectDataType
import logger as logging

from machine.config import MAX_NUM, MIN_NUM
from machine.register_controller import RegisterController
from model import Label, Labels, Memory, Opcode, Program, Register, ReturnCode
from util import parse_str_to_int

logger = logging.Logger('machine')


class DataPath:
    """
    DataPath class
    """
    def __init__(self, labels: Labels, memory: Memory) -> None:
        self.memory: List[Union[int, list]] = deepcopy(memory.memory)
        self.labels: Dict[str, int] = deepcopy(labels.labels)
        self.register_controller: RegisterController = RegisterController()
        self.acc: int = 0
        self.zero_flag: bool = False
        self.neg_flag: bool = False
        self.overflow_flag: bool = False
        self.output_buffer: List[int] = []
        self.data_register: int = 0
        self.instr_ptr: int = 0

    def put_operand(self, value: int) -> None:
        """
        Put operand into data_register
        :param value:
        :return:
        """
        self.data_register = value

    def inc(self) -> None:
        """
        Increment accumulator and set flags
        :return:
        """
        self.acc += 1
        self.get_flags()

    def dec(self) -> None:
        """
        Decrement accumulator and set flags
        :return:
        """
        self.acc -= 1
        self.get_flags()

    def add(self) -> None:
        """
        Add data_register into accumulator and set flags
        :return:
        """
        self.acc += self.data_register
        self.get_flags()

    def sub(self) -> None:
        """
        Subtract data_register into accumulator and set flags
        :return:
        """
        self.acc -= self.data_register
        self.get_flags()

    def mod(self) -> None:
        """
        Get module of accumulator and data_register and set flags
        :return:
        """
        self.acc %= self.data_register
        self.get_flags()

    def div(self) -> None:
        """
        Division accumulator by data_register and set flags
        :return:
        """
        self.acc //= self.data_register
        self.get_flags()

    def mul(self) -> None:
        """
        Multiply accumulator by data_register and set flags
        :return:
        """
        self.acc *= self.data_register
        self.data_register = 0
        self.get_flags()

    def get_flags(self) -> None:
        """
        Set flags
        :return:
        """
        self.neg_flag = self.acc < 0
        self.zero_flag = self.acc == 0
        self.overflow_flag = not MIN_NUM <= self.acc <= MAX_NUM

    def xor(self, dest: Any, operator: Any) -> None:
        """
        Perform XOR of two operands and set flags
        :param dest:
        :param operator:
        :return:
        """
        if isinstance(dest, Label):
            first_val = self.memory[self.labels[dest.label]]
        elif isinstance(dest, Register):
            first_val = self.register_controller.get(dest)
        elif isinstance(dest, int):
            first_val = self.memory[dest]
        else:
            raise IncorrectDataType
        if isinstance(operator, Label):
            second_val = self.memory[self.labels[operator.label]]  # type: ignore
        elif isinstance(operator, Register):
            second_val = self.register_controller.get(operator)  # type: ignore
        elif isinstance(operator, int):
            second_val = self.memory[operator]  # type: ignore
        else:
            raise IncorrectDataType
        self.set_acc(first_val ^ second_val)  # type: ignore
        self.wr(dest)
        self.get_flags()

    def wr(self, dest: Any) -> None:
        """
        Write accumulator by dest pointer
        :param dest:
        :return:
        """
        if isinstance(dest, Label):
            self.memory[self.labels[dest.label]] = self.acc
        elif isinstance(dest, Register):
            self.register_controller.set(dest, self.acc)
        elif isinstance(dest, int):
            self.memory[dest] = self.acc
        else:
            raise IncorrectDataType

    def jmp(self, dest: Any) -> None:
        """
        Perform jump operation
        :param dest:
        :return:
        """
        if isinstance(dest, Label):
            self.instr_ptr = self.labels[dest.label]
        else:
            raise IncorrectDataType

    def jmp_if(self, dest: Any, condition: bool) -> None:
        """
        Perform jump operation with condition
        :param dest:
        :param condition:
        :return:
        """
        if condition:
            self.jmp(dest)

    def inc_address_reg(self) -> None:
        """
        Increment address register
        :return:
        """
        self.instr_ptr += 1

    def set_acc(self, operand: Any) -> None:
        """
        Set operand's value into accumulator and set flags
        :param operand:
        :return:
        """
        if isinstance(operand, Label):
            self.acc = self.memory[self.labels[operand.label]]  # type: ignore
        elif isinstance(operand, Register):
            self.acc = self.register_controller.get(operand)
        elif isinstance(operand, int):
            self.acc = operand
        else:
            raise IncorrectDataType
        self.get_flags()

    def set_data_reg(self, operand: Any) -> None:
        """
        Set operand's value into data register and set flags
        :param operand:
        :return:
        """
        if isinstance(operand, Label):
            self.put_operand(self.memory[self.labels[operand.label]])  # type: ignore
        elif isinstance(operand, Register):
            self.put_operand(self.register_controller.get(operand))
        elif isinstance(operand, int):
            self.put_operand(operand)
        else:
            raise IncorrectDataType

    def put_by_dest(self,
                    dest: Any,
                    source: Any,
                    ) -> None:
        """
        Put source value into destination source
        :param dest:
        :param source:
        :return:
        """
        if isinstance(source, Register):
            source_value = self.register_controller.get(source)
        elif isinstance(source, Label):
            source_value = self.memory[self.labels[source.label]]  # type: ignore
        elif isinstance(source, int):
            source_value = source
        elif isinstance(source, str):
            source_value = ord(source)
        else:
            raise IncorrectDataType
        if isinstance(dest, Register):
            self.register_controller.set(dest, source_value)
        elif isinstance(dest, Label):
            self.memory[self.labels[dest.label]] = source_value
        elif isinstance(dest, int):
            self.memory[dest] = source_value
        else:
            raise IncorrectDataType


class ControlUnit:
    """
    ControlUnit class
    """
    def __init__(self, datapath: DataPath, input_data: List[Tuple[int, int]]) -> None:
        self.datapath = datapath
        self.input_data = input_data
        self.input_buffer_ptr = 0
        self.__tick = 0
        self.__instr = 0

    def instr(self) -> None:
        """
        Increment instr value
        :return:
        """
        self.__instr += 1

    def get_instr(self) -> int:
        """
        Return instr value
        :return:
        """
        return self.__instr

    def tick(self) -> None:
        """
        Increment tick value
        :return:
        """
        self.__tick += 1

    def get_tick(self) -> int:
        """
        Return tick value
        :return:
        """
        return self.__tick

    def get_value(self, operand: Union[Any]) -> int:
        """
        Get value from operand
        :param operand:
        :return:
        """
        if isinstance(operand, Label):
            return self.datapath.memory[Labels.labels[operand.label]]  # type: ignore
        if isinstance(operand, Register):
            return self.datapath.register_controller.get(operand)
        if isinstance(operand, int):
            return operand
        raise IncorrectDataType

    def get_symbol(self, ptr: int) -> ReturnCode:
        """
        Get symbol from input_buffer
        :param ptr:
        :return:
        """
        if ptr == len(self.input_data):
            self.datapath.set_acc(0)
            return ReturnCode.EOB
        if self.get_tick() >= self.input_data[ptr][0]:
            self.datapath.set_acc(self.input_data[ptr][1])
            return ReturnCode.SUCCESS
        return ReturnCode.WAIT

    def get_stdin(self) -> ReturnCode:
        """
        Method for reading stdin
        :return:
        """
        while (return_code := self.get_symbol(self.input_buffer_ptr)) != ReturnCode.SUCCESS:
            if return_code == ReturnCode.EOB:
                logger.info('Reach end of input buffer')
                return ReturnCode.EOB
            self.tick()
        self.input_buffer_ptr += 1
        logger.info('Get char from input buffer: %s', self.datapath.acc)
        return ReturnCode.SUCCESS

    def give_stdout(self, source: Union[Any]) -> None:
        """
        Put stdout into output_buffer
        :param source:
        :return:
        """
        if isinstance(source, Label):
            self.datapath.output_buffer.append(
                self.datapath.memory[self.datapath.labels[source.label]]  # type: ignore
            )
        elif isinstance(source, Register):
            self.datapath.output_buffer.append(self.datapath.register_controller.get(source))
        elif isinstance(source, int):
            self.datapath.output_buffer.append(source)
        else:
            raise IncorrectDataType

    def execute_instr(self,
                      operation: Opcode,
                      operands: List[Union[Label, Register, int, None]],
                      ) -> None:
        """
        Execute instruction
        :param operation:
        :param operands:
        :return:
        """
        self.instr()
        logger.debug('%s with %s', operation, operands)
        if operation == Opcode.INC:
            self.datapath.set_acc(operands[0])
            self.tick()
            self.datapath.inc()
            self.tick()
            self.datapath.wr(operands[0])
            self.tick()
        elif operation == Opcode.XOR:
            self.datapath.xor(operands[0], operands[1])
            self.tick()
        elif operation == Opcode.ADD:
            self.datapath.set_acc(operands[0])
            self.tick()
            self.datapath.set_data_reg(operands[1])
            self.tick()
            self.datapath.add()
            self.tick()
            self.datapath.wr(operands[0])
            self.tick()
        elif operation == Opcode.MOD:
            self.datapath.set_acc(operands[1])
            self.tick()
            self.datapath.set_data_reg(operands[2])
            self.tick()
            self.datapath.mod()
            self.tick()
            self.datapath.wr(operands[0])
            self.tick()
        elif operation == Opcode.MUL:
            self.datapath.set_acc(operands[0])
            self.tick()
            self.datapath.set_data_reg(operands[1])
            self.tick()
            self.datapath.mul()
            self.tick()
            self.datapath.wr(operands[0])
            self.tick()
        elif operation == Opcode.CMP:
            self.datapath.set_acc(operands[0])
            self.tick()
            self.datapath.set_data_reg(operands[1])
            self.tick()
            self.datapath.sub()
            self.tick()
        elif operation == Opcode.JMP:
            self.datapath.jmp(operands[0])
            self.tick()
        elif operation == Opcode.JE:
            self.datapath.jmp_if(operands[0], self.datapath.zero_flag)
            self.tick()
        elif operation == Opcode.JG:
            self.datapath.jmp_if(operands[0], self.datapath.neg_flag == self.datapath.overflow_flag
                                 and not self.datapath.zero_flag)
            self.tick()
        elif operation == Opcode.JNE:
            self.datapath.jmp_if(operands[0], not self.datapath.zero_flag)
            self.tick()
        elif operation == Opcode.MOV:
            if isinstance(operands[0], Label) and operands[0].label.upper() == 'STDOUT':
                self.give_stdout(operands[1])
            elif isinstance(operands[1], Label) and operands[1].label.upper() == 'STDIN':
                ret_code = self.get_stdin()
                self.datapath.wr(operands[0])
            else:
                if isinstance(operands[1], Label) and operands[1].indirect_ptr:
                    indirect_ptr = self.get_value(operands[1].indirect_ptr)
                    label_ptr = self.datapath.labels[operands[1].label]
                    if indirect_ptr == len(self.datapath.memory[label_ptr]):  # type: ignore
                        self.datapath.put_by_dest(operands[0], 0)
                    else:
                        self.datapath.put_by_dest(
                            operands[0],
                            self.datapath.memory[label_ptr][indirect_ptr]  # type: ignore
                        )
                else:
                    self.datapath.put_by_dest(operands[0], operands[1])
            self.tick()
        elif operation == Opcode.HLT:
            raise ProgramExit

    def execute_program(self, program: Program) -> None:
        """
        Execute program
        :param program:
        :return:
        """
        try:
            while True:
                instruction = program.instructions[self.datapath.instr_ptr]
                self.datapath.inc_address_reg()
                for cmd in Opcode:
                    if cmd.name.lower() == instruction.cmd.lower():
                        self.execute_instr(cmd, instruction.operands)
        except ProgramExit:
            return

    def return_output_buffer(self) -> List[int]:
        """
        Return output buffer
        :return:
        """
        return self.datapath.output_buffer


def get_input_data(input_path: str) -> List[Tuple[int, int]]:
    """
    Read file and return input_data
    :param input_path:
    :return:
    """
    input_data: List[Tuple[int, int]] = []
    with open(input_path, 'r', encoding='utf-8') as f:
        while line := f.readline():
            tick, value = map(parse_str_to_int, line.split(','))
            input_data.append((tick, value))
    return input_data


def execute(program: Program, memory: Memory, labels: Labels, input_path: str = '') -> List[int]:
    """
    Execute method for external execute
    :param program:
    :param memory:
    :param labels:
    :param input_path:
    :return:
    """
    datapath: DataPath = DataPath(labels, memory)
    if input_path:
        input_data = get_input_data(input_path)
    else:
        input_data = []
    control_unit: ControlUnit = ControlUnit(datapath, input_data)
    control_unit.execute_program(program)
    logger.info('tick: %s ; instr: %s', control_unit.get_tick(), control_unit.get_instr())
    return control_unit.return_output_buffer()

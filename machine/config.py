"""
Config for .asm
"""
from typing import List

LOGGING_CONFIG = {
    'version': 1,
    'formatters': {
        'translator': {
            'format':
                '%(asctime)s %(processName)-11s %(process)-5d [%(levelname)-8s] '
                '%(name)s: %(message)s',
        },
    },
    'handlers': {
        'machine': {
            'class': 'logging.FileHandler',
            'filename': 'program.log',
            'formatter': 'translator',
        },
        'cli': {
            'class': 'logging.FileHandler',
            'filename': 'program.log',
            'formatter': 'translator',
        },
        'full_scope': {
            'class': 'logging.FileHandler',
            'filename': 'program.log',
            'formatter': 'translator',
        }
    },
    'loggers': {
        'machine': {
            'handlers': ['machine'],
            'level': 'DEBUG',
        },
        'cli': {
            'handlers': ['cli'],
            'level': 'DEBUG',
        },
        'default': {
            'handlers': ['full_scope'],
            'level': 'INFO',
        }
    },
}

__available_registers__: List[str] = ['rax', 'rbx', 'rdx', 'rsi', 'rdi', 'rsx']

MEM_SIZE = 10
MAX_NUM = 1024
MIN_NUM = -1024

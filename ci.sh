#!/bin/bash

echo '=====PYLINT====='
pylint ./cli.py ./model.py ./translator.py ./util.py ./machine/
echo '======MYPY======'
mypy .

echo '==[TESTING]=='
echo '====UNIT-TESTS===='
python -m unittest test/unit/test_translator.py
python -m unittest test/unit/test_machine.py
python -m unittest test/integration/test_prob5.py
python -m unittest test/integration/test_cat.py
python -m unittest test/integration/test_hello.py
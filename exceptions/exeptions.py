"""
Custom exceptions
"""


class PyAsmException(Exception):
    """
    Base Exception class
    """


class IncorrectDataType(PyAsmException):
    """
    Raised when needed data has another type
    """


class ProgramExit(PyAsmException):
    """
    End of program
    """

"""
Translator
"""
# pylint: disable=global-statement
# pylint: disable=no-else-return
# pylint: disable=global-variable-not-assigned
# pylint: disable=invalid-name
from typing import List, Tuple, Union
from exceptions.exeptions import IncorrectDataType
import logger as logging
from machine.config import __available_registers__
from model import Instruction, Label, Labels, Memory, Program, Register
from util import is_string

logger = logging.Logger('')
PTR = -1


def clear_ptr() -> None:
    """
    Clear pointer
    :return:
    """
    global PTR
    PTR = -1


def inc_ptr() -> int:
    """
    Increment pointer
    :return:
    """
    global PTR
    PTR += 1
    return PTR


def get_ptr() -> int:
    """
    Get pointer
    :return:
    """
    global PTR
    return PTR + 1


def parse_code(text: str) -> Tuple[Program, Memory, Labels]:
    """
    Parse code
    :param text:
    :return:
    """
    data_ptr = text.find('.data')
    code_ptr = text.find('.text')
    data_section = list(map(str.strip, text[data_ptr:code_ptr].splitlines()[1::]))
    text_section = list(map(str.strip, text[code_ptr::].splitlines()[1::]))
    for line in data_section:
        if ':' in line:
            parse_data_line(line, inc_ptr())
    clear_ptr()
    for line in text_section:
        label = ''
        instruction = line
        if ':' in line:
            try:
                label, instruction = line.split(':', 1)
            except ValueError:
                label = line
                instruction = ''
        if label:
            if label[0] == '.':
                label = label[1::]
            add_label(label.strip(), get_ptr())
        if instruction:
            add_code_line(instruction.strip(), inc_ptr())
    return Program(), Memory(), Labels()


def add_code_line(instruction: str, addr: int) -> None:
    """
    Add code line into program list
    :param instruction:
    :param addr:
    :return:
    """
    Program.instructions.append(parse_instruction(instruction, addr))


def add_label(label: str, addr: int) -> None:
    """
    Add label into labels list
    :param label:
    :param addr:
    :return:
    """
    Labels.labels[Label(label).label] = addr


def parse_data_line(line: str, addr: int) -> None:
    """
    Parse data line
    :param line:
    :param addr:
    :return:
    """
    label, value = map(str.strip, line.split(':'))
    if is_string(value):
        list_value = list(value[1:-1])
        Memory.memory[addr] = list_value
    else:
        Memory.memory[addr] = int(value, 0)
    Labels.labels[Label(label).label] = addr


def parse_instruction(line: str, addr: int) -> Instruction:
    """
    Parse instruction
    :param line:
    :param addr:
    :return:
    """
    try:
        cmd, operands = map(str.strip, line.split(' ', 1))
    except ValueError:
        cmd = line
        operands = ''
    return Instruction(cmd, parse_operands(operands), addr)


def parse_operands(line: str) -> List[Union[Label, Register, int, None]]:
    """
    Parse operands
    :param line:
    :return:
    """
    if not line:
        return [None]
    operands = list(map(str.strip, line.split(',')))
    return list(map(parse_operand, operands))


def parse_operand(operand: str) -> Union[Label, Register, int]:
    """
    Parse operand
    :param operand:
    :return:
    """
    if operand[0] == '#' or operand[0] == '.':
        if '[' in operand:
            operand, ptr = operand.split('[')
            ptr_val = parse_operand(ptr[:-1])
            return Label(operand[1::], ptr_val)
        return Label(operand[1::].strip())
    elif operand[0] == '%':
        if operand[1::] in __available_registers__:
            return Register(operand[1::])
        raise IncorrectDataType
    return int(operand, 0)

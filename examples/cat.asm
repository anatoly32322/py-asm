section .data
    NULL_TERM: 0x00
    X: 0x00

section .text
    .read_char:
        ADD #X, 0x5
        MOV %rsx, #STDIN
        CMP %rsx, #NULL_TERM
        JE .exit
        MOV #STDOUT, %rsx
        JMP .read_char
    .exit:
        HLT

"""
Logging module.
"""
import logging
import logging.config
from dataclasses import dataclass


def configure(config: dict) -> None:
    """
    Configure logging.
    """
    logging.config.dictConfig(config)


@dataclass
class Logger:
    process_file_name: str

    def error(self, msg, *args, **kwargs):
        """
        Log a message with severity 'ERROR'.
        """
        self._get_logger().error(msg, *args, **kwargs)

    def exception(self, msg, *args, **kwargs):
        """
        Log a message with severity 'ERROR' with exception information.
        """
        self._get_logger().exception(msg, *args, **kwargs)

    def warning(self, msg, *args, **kwargs):
        """
        Log a message with severity 'WARNING'.
        """
        self._get_logger().warning(msg, *args, **kwargs)

    def info(self, msg, *args, **kwargs):
        """
        Log a message with severity 'INFO'.
        """
        self._get_logger().info(msg, *args, **kwargs)

    def debug(self, msg, *args, **kwargs) -> None:
        """
        Log a message with severity 'DEBUG'.
        """
        self._get_logger().debug(msg, *args, **kwargs)

    def _get_logger(self) -> logging.Logger:
        try:
            logger = logging.getLogger(self.process_file_name)
        except ValueError:
            logger = logging.getLogger('default')
        return logger

"""
Command-line interface
"""
# pylint: disable=unspecified-encoding
# pylint: disable=invalid-name
from typing import List
from click import argument, command, group

import logger as logging
from machine.config import LOGGING_CONFIG
from machine.machine import execute as machine_execute
from translator.preprocessing import minify_text
from translator.translator import parse_code
from util import (delete_object_files, read_labels, read_memory, read_program,
                  write_labels, write_memory, write_program)

logger = logging.Logger('cli')


def translate(path: str) -> None:
    """
    Translate .asm file
    :param path:
    :return:
    """
    logger.info('Translate program into object file')
    with open(path, 'r', encoding='utf-8') as asm_file:
        asm_text = asm_file.read()
        program, memory, labels = parse_code(minify_text(asm_text))
    write_program(program, path)
    write_memory(memory, path)
    write_labels(labels, path)


def execute(path: str, input_path: str) -> List[int]:
    """
    Read and execute object files
    :param path:
    :param input_path:
    :return:
    """
    logger.info('Execute program from object file')
    program = read_program(path)
    memory = read_memory(path)
    labels = read_labels(path)
    output_buffer = machine_execute(program, memory, labels, input_path)
    delete_object_files(path)
    return output_buffer


@command(name='translator')
@argument('path', metavar='<path>')
def translator(path: str) -> None:
    """
    Translate command
    :param path:
    :return:
    """
    translate(path)


@command(name='execute')
@argument('path', metavar='<path>')
@argument('input_path', default='', metavar='<input_path>')
def executer(path: str, input_path: str) -> None:
    """
    Execute command
    :param path:
    :param input_path:
    :return:
    """
    output_buffer = execute(path, input_path)
    print(output_buffer)


@command(name='run')
@argument('path', metavar='<path>')
@argument('input_path', default='', metavar='<input_path>')
def run(path: str, input_path: str) -> None:
    """
    Run command
    :param path:
    :param input_path:
    :return:
    """
    translate(path)
    output_buffer = execute(path, input_path)
    print(output_buffer)


@group()
def main():
    """
    Group method
    :return:
    """
    logging.configure(LOGGING_CONFIG)


main.add_command(translator)
main.add_command(executer)
main.add_command(run)


if __name__ == '__main__':
    main()

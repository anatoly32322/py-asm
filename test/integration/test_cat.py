"""
Integration test for cat.asm
"""
import sys
import unittest
import subprocess
from typing import List


class TestCat(unittest.TestCase):
    """
    TestCat class
    """
    def test_output(self) -> None:
        """
        Test cat.asm
        :return:
        """
        filepath: str = 'examples/cat.asm'
        input_path: str = 'examples/input/cat.txt'
        command: List[str] = [sys.executable, 'cli.py', 'run', filepath, input_path]
        result: str = subprocess.check_output(command).decode().strip()
        self.assertEqual(result, '[102, 111, 111]')

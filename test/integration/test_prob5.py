"""
Integration test for prob5
"""
import sys
import unittest
import subprocess
from typing import List


class TestProb5(unittest.TestCase):
    """
    TestProb5 class
    """
    def test_output_01(self) -> None:
        """
        Test prob5 with stdin = 6
        :return:
        """
        filepath: str = 'examples/prob5.asm'
        input_path: str = 'examples/input/input_01.txt'
        command: List[str] = [sys.executable, 'cli.py', 'run', filepath, input_path]
        result: str = subprocess.check_output(command).decode().strip()
        self.assertEqual(result, '[60]')

    def test_output_02(self) -> None:
        """
        Test prob5 with stdin = 7
        :return:
        """
        filepath: str = 'examples/prob5.asm'
        input_path: str = 'examples/input/input_02.txt'
        command: List[str] = [sys.executable, 'cli.py', 'run', filepath, input_path]
        result: str = subprocess.check_output(command).decode().strip()
        self.assertEqual(result, '[420]')

    def test_output_03(self) -> None:
        """
        Test prob5 with stdin = 10
        :return:
        """
        filepath: str = 'examples/prob5.asm'
        input_path: str = 'examples/input/input_03.txt'
        command: List[str] = [sys.executable, 'cli.py', 'run', filepath, input_path]
        result: str = subprocess.check_output(command).decode().strip()
        self.assertEqual(result, '[2520]')

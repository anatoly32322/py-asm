"""
Integration test for hello.asm
"""
import sys
import unittest
import subprocess
from typing import List


class TestHello(unittest.TestCase):
    """
    TestHello class
    """
    def test_output(self) -> None:
        """
        Test hello.asm
        :return:
        """
        filepath: str = 'examples/hello.asm'
        command: List[str] = [sys.executable, 'cli.py', 'run', filepath]
        result: str = subprocess.check_output(command).decode().strip()
        self.assertEqual(result, '[104, 101, 108, 108, 111, 32, 119, 111, 114, 108, 100]')

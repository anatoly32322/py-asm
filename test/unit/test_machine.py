"""
Test machine
"""
import unittest

from machine.machine import execute as execute_machine
from model import Instruction, Label, Labels, Memory, Program


class TestMachine(unittest.TestCase):
    """
    TestMachine class
    """
    def test_machine_01(self) -> None:
        """
        Test ADD operation
        :return:
        """
        Program().instructions = [
            Instruction(cmd='ADD', operands=[Label(label='X'), Label(label='Y')], addr=0),
            Instruction(cmd='MOV', operands=[Label(label='STDOUT'), Label(label='X')], addr=1),
            Instruction(cmd='HLT', operands=[None], addr=2),
        ]
        Labels().labels = {
            'X': 0,
            'Y': 1
        }
        Memory.memory = [1, 2, 0, 0, 0, 0, 0, 0, 0, 0]
        output = execute_machine(Program(), Memory(), Labels())
        self.assertEqual(output, [3])

    def test_machine_02(self) -> None:
        """
        Test MUL operation
        :return:
        """
        Program().instructions = [
            Instruction(cmd='MUL', operands=[Label(label='X'), Label(label='Y')], addr=0),
            Instruction(cmd='MOV', operands=[Label(label='STDOUT'), Label(label='X')], addr=1),
            Instruction(cmd='HLT', operands=[None], addr=2),
        ]
        Labels().labels = {
            'X': 0,
            'Y': 1,
        }
        Memory.memory = [2, 3, 0, 0, 0, 0, 0, 0, 0, 0]
        output = execute_machine(Program(), Memory(), Labels())
        self.assertEqual(output, [6])

    def test_machine_03(self) -> None:
        """
        Test MOD operation
        :return:
        """
        Program().instructions = [
            Instruction(cmd='MOD', operands=[Label(label='RESULT'), Label(label='X'), Label(label='Y')], addr=0),
            Instruction(cmd='MOV', operands=[Label(label='STDOUT'), Label(label='RESULT')], addr=1),
            Instruction(cmd='HLT', operands=[None], addr=2),
        ]
        Labels().labels = {
            'X': 0,
            'Y': 1,
            'RESULT': 2,
        }
        Memory.memory = [6, 2, 0, 0, 0, 0, 0, 0, 0, 0]
        output = execute_machine(Program(), Memory(), Labels())
        self.assertEqual(output, [0])

"""
Test translator
"""
import unittest

from model import Instruction, Label
from translator.translator import parse_code


class TestTranslator(unittest.TestCase):
    """
    TestTranslator class
    """
    def test_translator_01(self) -> None:
        """
        Test translator with ADD operation
        :return:
        """
        with open('examples/test_01.asm', 'r') as test_01:
            program, _, labels = parse_code(test_01.read())
        self.assertEqual(program.instructions, [
            Instruction(cmd='ADD', operands=[Label(label='X'), Label(label='Y')], addr=0),
            Instruction(cmd='MOV', operands=[Label(label='STDOUT'), Label(label='X')], addr=1),
            Instruction(cmd='HLT', operands=[None], addr=2),
        ])
        self.assertEqual(labels.labels, {
            'X': 0,
            'Y': 1
        })

"""
Models
"""
# pylint: disable=too-few-public-methods
from dataclasses import dataclass
from enum import Enum
from typing import Dict, List, Union

from machine.config import MEM_SIZE


class Opcode(Enum):
    """
    Opcode enum
    """
    XOR = 'xor'
    ADD = 'add'  # ACC + X -> ACC
    INC = 'inc'  # ACC + 1 -> ACC
    MOD = 'mod'  # ACC % X -> ACC
    MUL = 'mul'  # ACC * X -> ACC
    CMP = 'cmp'  # ACC -> NEG, ZERO
    JE = 'je'  # A == B
    JG = 'jg'  # A > B
    JMP = 'jmp'  # X -> IP
    JNE = 'jne'  # A != B
    MOV = 'mov'  # B -> A
    HLT = 'hlt'  # HLT


class ReturnCode(Enum):
    """
    Return codes enum
    """
    SUCCESS = 1
    WAIT = 2
    EOB = 0


@dataclass
class Register:
    """
    Register class
    """
    name: str

    def __str__(self):
        return f'%{self.name}'


@dataclass
class Label:
    """
    Label class
    """
    label: str
    indirect_ptr: Union[Register, 'Label', int, None] = None

    def __str__(self):
        return self.label

    def __hash__(self):
        return hash(self.label)


class Memory:
    """
    Memory class
    """
    memory: List[Union[int, list]] = [0] * MEM_SIZE

    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super(Memory, cls).__new__(cls)
        return cls.instance


class Labels:
    """
    Labels class
    """
    labels: Dict[str, int] = {}

    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super(Labels, cls).__new__(cls)
        return cls.instance


@dataclass
class Instruction:
    """
    Instruction class
    """
    cmd: str
    operands: List[Union[Label, Register, int, None]]
    addr: int

    def __str__(self):
        return f'{self.addr}: {self.cmd} {self.operands}'


class Program:
    """
    Program class
    """
    instructions: List[Instruction] = []

    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super(Program, cls).__new__(cls)
        return cls.instance
